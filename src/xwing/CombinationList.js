import React from 'react';


import { CombinationListItem } from './CombinationListItem';


export const CombinationList = (props) => {
  const combinations = props.combinations;
  return combinations
    .map((combination, index) => {
      if (combination.sides) {
        return (
          <CombinationListItem
            key={ index }
            sides={ combination.sides }
            length={ combinations.length }
            index={ index } />
        );
      } else {
        return (
          <CombinationListItem
            key={ index }
            sides={ combination.map(c => c.name) }
            length={ combinations.length }
            index={ index } />
        );
      }
    })
}
