import React from 'react';


import { TotalHit } from './TotalHit';


export const TotalHits = (props) => {
  const maxCount = Math.max(...props.totalHits.map(c => c.count));
  const totalHits = props.totalHits.map((totalHit, index) => (
    <TotalHit
      key={ index }
      totalHit={ totalHit }
      maxCount={ maxCount } />
  ));
  return totalHits;
}
