import React from 'react';


import { TotalHits } from './TotalHits';
import { AppTitle } from './AppTitle';


export class GroupedByTotalHits extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      totalHits: []
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.numberOfDice !== nextProps.numberOfDice) {
      this.loadData(nextProps.numberOfDice);
    }
  }

  componentDidMount() {
    this.loadData(this.props.numberOfDice);
  }

  loadData(count) {
    const entity = "total_hits";
    const url = "/api/xwing/attack_dice_combinations/collected_<entity>/<count>"
      .replace("<entity>", entity)
      .replace("<count>", count.toString());

    fetch(url)
      .then(response => response.json())
      .then(data => this.setState({ totalHits: data }));
  }

  render() {
    return (
      <div>
        <div className="combination-header-container">
          <AppTitle
            title="Combinations Grouped by Total Hits"
            number={ this.state.totalHits.length } />
          <p className="combination-text ms-font-l">
          </p>
          <p className="combination-text ms-font-l">
            All outcomes are diplayed below:
          </p>
          <TotalHits totalHits={ this.state.totalHits } />
        </div>
      </div>
    );
  }
}
