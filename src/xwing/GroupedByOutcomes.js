import React from 'react';


import { AppTitle } from './AppTitle';
import { Outcomes } from './Outcomes';


import './Combinations.css';


export class GroupedByOutcomes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      outcomes: []
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.numberOfDice !== nextProps.numberOfDice) {
      this.loadData(nextProps.numberOfDice);
    }
  }

  componentDidMount() {
    this.loadData(this.props.numberOfDice);
  }

  loadData(count) {
    const entity = "outcomes";
    const url = "/api/xwing/attack_dice_combinations/collected_<entity>/<count>"
      .replace("<entity>", entity)
      .replace("<count>", count.toString());

    fetch(url)
      .then(response => response.json())
      .then(data => this.setState({ outcomes: data }));
  }

  createText(outcomes, diceCount) {
    return ("We are mostly interested in hits and crits when attacking. " +
            "If we count only hits and crits as success, we get <outcomes> " +
            "outcomes when rolling <diceCount> attack dice.")
            .replace("<outcomes>", outcomes)
            .replace("<diceCount>", diceCount);
  }

  render() {
    const outcomes = this.state.outcomes;
    const outcomeTotal = outcomes.length;
    const numberOfDice = this.props.numberOfDice;
    const text = this.createText(outcomeTotal, numberOfDice);
    return (
      <div>
        <div className="combination-header-container">
          <AppTitle
            title="Combinations Grouped by Hits and Crits"
            number={ outcomeTotal } />
          <p className="combination-text ms-font-l">
            { text }
          </p>
          <p className="combination-text ms-font-l">
            All outcomes are diplayed below:
          </p>
          <Outcomes outcomes={ outcomes } />
        </div>
      </div>
    );
  }
}
