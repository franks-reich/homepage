import React from 'react';


import { Callout } from 'office-ui-fabric-react/lib/Callout';


import { DiceList } from './DiceList';
import { CombinationList } from './CombinationList';


export class Summary extends React.Component {
  constructor(props) {
    super(props);
    this.summary = null;
    this.state = {
      isCalloutOpen: false
    }
    this.onCalloutOpen = this.onCalloutOpen.bind(this);
  }

  onCalloutOpen() {
    this.setState({
      isCalloutOpen: !this.state.isCalloutOpen
    });
  }

  render() {
    const isCalloutOpen = this.state.isCalloutOpen;
    const item = this.props.item;
    const diceList = (() => {
      if (isCalloutOpen) {
        if (item.sides) {
          return <DiceList sides={ item.sides } />;
        } else if (item.combinations) {
          return <CombinationList combinations={ item.combinations } />;
        }
      }
      return null;
    })();
    return (
      <div className="combination-summary"
          onClick={ this.onCalloutOpen }
          ref={ (s) => this.summary = s }>
        <div
            className="combination-summary-container">
          <p className="combination-summary-title ms-font-m-plus">Hits</p>
          <p className="combination-summary-count ms-font-xxl">{ item.numberOfHits }</p>
        </div>
        <div
            className="combination-summary-container">
          <p className="combination-summary-title ms-font-m-plus">Crits</p>
          <p className="combination-summary-count ms-font-xxl">{ item.numberOfCrits }</p>
        </div>
        <div
            className="combination-summary-total-container">
          <p className="combination-summary-title ms-font-m-plus">Total</p>
          <p className="combination-summary-count ms-font-xxl">{ item.total }</p>
        </div>
        { isCalloutOpen && (
          <Callout
            className='combination-details-callout'
            role={ 'alertdialog' }
            gapSpace={ 0 }
            target={ this.summary }
            setInitialFocus={ true }>
            <div className="combination-container">
              <div className="dice-container">
                { diceList }
              </div>
            </div>
          </Callout>
        )}
      </div>
    );
  }
}
