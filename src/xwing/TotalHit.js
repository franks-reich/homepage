import React from 'react';


import { GraphBar } from './GraphBar';


export const TotalHit = (props) => {
  const totalHit = props.totalHit;
  const maxCount = props.maxCount;
  return (
    <div className="combination-container">
      <GraphBar value={ totalHit.count } max={ maxCount } />
      <div className="combination-summary">
        <div
            className="combination-summary-container">
          <p className="combination-summary-title ms-font-m-plus">Total</p>
          <p className="combination-summary-count ms-font-xxl">{ totalHit.outcome }</p>
        </div>
      </div>
    </div>
  );
}
