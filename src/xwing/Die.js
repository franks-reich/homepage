import React from 'react';


import './Die.css';


export const Die = (props) => (
  <div className="die-side-div ms-bgColor-themeLighter">
    <div className="die-side-text ms-font-l">{ props.text }</div>
  </div>
)
