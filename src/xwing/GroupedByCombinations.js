import React from 'react';


import { Combination } from './Combination';
import { CombinationList } from './CombinationList';
import { AppTitle } from './AppTitle';


import './Combinations.css';


export class GroupedByCombinations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      combinations: []
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.numberOfDice !== nextProps.numberOfDice) {
      this.loadData(nextProps.numberOfDice);
    }
  }

  componentDidMount() {
    this.loadData(this.props.numberOfDice);
  }

  loadData(count) {
    const entity = "combinations";
    const url = "/api/xwing/attack_dice_combinations/collected_<entity>/<count>"
      .replace("<entity>", entity)
      .replace("<count>", count.toString());

    fetch(url)
      .then(response => response.json())
      .then(data => this.setState({ combinations: data }));
  }

  mapCombinations(combinations) {
    return combinations.map((combination, index) => {
      const sides = combination.combination.map((side) => side.name);
      const numberOfHits = sides.filter(s => s === "Hit").length;
      const numberOfCrits = sides.filter(s => s === "Crit").length;
      const total = numberOfHits + numberOfCrits;    
      return ({
        sides: sides,
        numberOfHits: numberOfHits,
        numberOfCrits: numberOfCrits,
        total: total,
        count: combination.count
      });
    });
  }

  createCombinationsElements(combinations, max) {
    return combinations.map((item, index) => (
      <Combination
        key={ index }
        max={ max }
        item={ item } />
    ));
  }

  calculateMax(combinations) {
    const combinationCounts = combinations.map(c => c.count);
    return Math.max(...combinationCounts);
  }

  calculateTotal(combinations) {
    return combinations
      .reduce((total, combination) => (total + combination.count), 0);
  }

  render() {
    const combinations = this.mapCombinations(this.state.combinations);
    const max = this.calculateMax(combinations);
    const combinationsElements = this.createCombinationsElements(combinations, max);
    const combinationTotal = this.calculateTotal(combinations);
    const numberOfDice = this.props.numberOfDice;
    const text = "Rolling <attackDiceCount> attack dice creates <combinationCount> combinations."
      .replace("<attackDiceCount>", numberOfDice)
      .replace("<combinationCount>", combinationTotal);
    const combinationsWithMaximumCount = combinations
      .filter((c) => (c.count === max));
    return (
      <div>
        <div className="combination-header-container">
          <AppTitle
            title="Combinations"
            number={ combinationTotal } />
          <p className="combination-text ms-font-l">
            { text }
          </p>
          <p className="combination-text ms-font-l">
            We disregard the order of each combination and only count the sides
            of the die. Then we group each combination with the same die sides,
            we can count how often we hit the same outcome in the game. The
            following outcomes come up the most:
          </p>
          <div className="combination-text-outcomes-container">
            <CombinationList
              combinations={ combinationsWithMaximumCount } />
          </div>
          <p className="combination-text ms-font-l">
            All outcomes are diplayed below:
          </p>
        </div>
        { combinationsElements }
      </div>
    );
  }
}
