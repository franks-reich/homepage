import React from 'react';


import { Link } from 'react-router-dom';


import { ChoiceGroup } from 'office-ui-fabric-react/lib/ChoiceGroup';
import { Slider } from 'office-ui-fabric-react/lib/Slider';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';


export class AttackDiceCommandBar extends React.Component {
  createNumberOfDiceValueElement (
      numberOfDice,
      onNumberOfDiceTextViewChanged) {
    const style = { textAlign: "center" };
    const textField = 
      <TextField
        className="ms-font-su ms-bgColor-neutralLight"
        style={ style }
        value={ numberOfDice }
        onChanged={ onNumberOfDiceTextViewChanged } />
    return textField;
  }

  render () {
    const groupBy = this.props.groupBy;
    const numberOfDice = this.props.numberOfDice;
    const navigationUrl = "/IDF/Training/XWingAttackDice/<groupBy>/<count>"
      .replace("<count>", numberOfDice)
      .replace("<groupBy>", groupBy);
    const onGroupingChoiceGroupChange = this.props.onGroupingChoiceGroupChange;
    const onNumberOfDiceSlideChange = this.props.onNumberOfDiceSliderChange;
    const onNumberOfDiceTextViewChanged = this.props.onNumberOfDiceTextViewChanged;
    const numberOfDiceValueElement = 
      this.createNumberOfDiceValueElement(
        numberOfDice, onNumberOfDiceTextViewChanged);
    return (
      <div>
        <div className="attack-dice-tools ms-bgColor-neutralLight">
          <ChoiceGroup
            selectedKey={ groupBy }
            label="Group Combinations by ..."
            options={ [
              {
                key: "total_hits",
                iconProps: { iconName: "12PointStar" },
                text: "Total Hits"
              },
              {
                key: "outcomes",
                iconProps: { iconName: "12PointStar" },
                text: "Hits and Crits"
              },
              {
                key: "combinations",
                iconProps: { iconName: "More" },
                text: "Combinations"
              }
            ] }
            onChange={ onGroupingChoiceGroupChange }
          />
          <div className="attack-dice-toolbar-space" />
          <div className="attack-dice-number-of-dice">
            <Slider label="Number of Dice"
                    min={ 1 }
                    max={ 6 }
                    value={ numberOfDice }
                    vertical={ false }
                    showValue={ false }
                    onChange={ onNumberOfDiceSlideChange } />
            { numberOfDiceValueElement }
          </div>
        </div>
        <div className="ms-bgColor-neutralLighter">
          <Link to={ navigationUrl }>
            <DefaultButton primary
                           className="attack-dice-toolbar-button"
                           text="Update" />
          </Link>
        </div>
      </div>
    );
  }
}
