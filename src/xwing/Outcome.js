import React from 'react';


import { Summary } from './Summary';
import { GraphBar } from './GraphBar';


export const Outcome = (props) => {
  const maxCount = props.maxCount;
  const outcome = {
    numberOfHits: props.outcome.outcome.hits,
    numberOfCrits: props.outcome.outcome.crits,
    total: props.outcome.outcome.hits + props.outcome.outcome.crits,
    count: props.outcome.count,
    combinations: props.outcome.combinations
  };
  return (
    <div className="combination-container">
      <GraphBar value={ outcome.count } max={ maxCount } />
      <Summary item={ outcome } />
    </div>
  );
} 
