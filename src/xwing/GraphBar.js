import React from 'react';


export const GraphBar = (props) => {
  const max = props.max;
  const value = props.value;
  const maxHeight = 15.0;
  const height = (value / max) * maxHeight;
  const style = {
     height: height.toString() + "em",
     marginTop: (maxHeight - height).toString() + "em"
  };
  const outerStyle = { height: maxHeight.toString() + "em" };
  const textClass = (() => {
    if (max.toString().length > 3) {  
      return "combination-count-text ms-font-xl ms-fontColor-purple";
    } else if (max.toString().length > 2) {
      return "combination-count-text ms-font-xxl ms-fontColor-purple";
    } else {
      return "combination-count-text ms-font-su ms-fontColor-purple";
    }
  })();
  return (
    <React.Fragment>
      <p className={ textClass }>
        { value }
      </p>
      <div className="combination-graph"
          style={ outerStyle }>
        <div className="combination-graph-bar ms-bgColor-purple ms-borderColor-purpleDark"
            style={ style } />
      </div>
    </React.Fragment>
  );
}
