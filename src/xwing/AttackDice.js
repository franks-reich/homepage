import React from 'react';


import { Route, Redirect } from 'react-router-dom';


import { GroupedByCombinations } from './GroupedByCombinations';
import { GroupedByOutcomes } from './GroupedByOutcomes';
import { GroupedByTotalHits } from './GroupedByTotalHits';
import { AttackDiceCommandBar } from './AttackDiceCommandBar';


import './AttackDice.css';


export class AttackDice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      combinations: [],
      groupCombinationsBy: "",
      numberOfDiceValueEditable: false
    };

    this.onGroupingChoiceGroupChange = this.onGroupingChoiceGroupChange.bind(this);
    this.onNumberOfDiceSlideChange = this.onNumberOfDiceSlideChange.bind(this);
    this.onNumberOfDiceValueClicked = this.onNumberOfDiceValueClicked.bind(this);
    this.onNumberOfDiceTextViewChanged = this.onNumberOfDiceTextViewChanged.bind(this);
  }

  onNumberOfDiceValueClicked(event) {
    const editable = !this.state.numberOfDiceValueEditable;
    this.setState({ numberOfDiceValueEditable: editable });
  }

  onNumberOfDiceSlideChange(value) {
    this.setState({ numberOfDice: value });
  }

  onGroupingChoiceGroupChange(event, option) {
    this.setState({ groupCombinationsBy: option.key });
  }

  onNumberOfDiceTextViewChanged(value) {
    const number = parseInt(value, 10);
    if (!isNaN(number) && number < 7) {
      this.setState({ numberOfDice: number });
    }
  }

  getNumberOfDice(number) {
    if (!this.state.numberOfDice) {
      return number;
    } else {
      return this.state.numberOfDice;
    }
  }

  getGroupBy(groupBy) {
    if (this.state.groupCombinationsBy === "") {
      return groupBy;
    } else {
      return this.state.groupCombinationsBy;
    }
  }

  render() {
    return (
      <div className="attack-dice-app">
        <Route
          path="/IDF/Training/XWingAttackDice/total_hits/:count"
          render={
          (props) => {
            const number = parseInt(props.match.params.count, 10);
            if (!isNaN(number)) {
              const numberOfDice = this.getNumberOfDice(number);
              const groupBy = this.getGroupBy("total_hits");
              return (
                <div>
                  <AttackDiceCommandBar
                    groupBy={ groupBy }
                    numberOfDice={ numberOfDice }
                    onGroupingChoiceGroupChange={ this.onGroupingChoiceGroupChange }
                    onNumberOfDiceSliderChange={ this.onNumberOfDiceSlideChange }
                    onNumberOfDiceTextViewChanged={ this.onNumberOfDiceTextViewChanged } />
                  <GroupedByTotalHits numberOfDice={ number } />
                </div>
              );
            } else {
              return null;
            }
          }} />
        <Route
          path="/IDF/Training/XWingAttackDice/outcomes/:count"
          render={
          (props) => {
            const number = parseInt(props.match.params.count, 10);
            if (!isNaN(number)) {
              const numberOfDice = this.getNumberOfDice(number);
              const groupBy = this.getGroupBy("outcomes");
              return (
                <div>
                  <AttackDiceCommandBar
                    groupBy={ groupBy }
                    numberOfDice={ numberOfDice }
                    onGroupingChoiceGroupChange={ this.onGroupingChoiceGroupChange }
                    onNumberOfDiceSliderChange={ this.onNumberOfDiceSlideChange }
                    onNumberOfDiceTextViewChanged={ this.onNumberOfDiceTextViewChanged } />
                  <GroupedByOutcomes numberOfDice={ number } />
                </div>
              );
            } else {
              return null;
            }
          }} />
        <Route
          exact
          path="/IDF/Training/XWingAttackDice/combinations/:count"
          render={
            (props) => {
              const number = parseInt(props.match.params.count, 10);
              if (!isNaN(number)) {
                const numberOfDice = this.getNumberOfDice(number);
                const groupBy = this.getGroupBy("combinations");
                return (
                  <div>
                    <AttackDiceCommandBar
                      groupBy={ groupBy }
                      numberOfDice={ numberOfDice }
                      onGroupingChoiceGroupChange={ this.onGroupingChoiceGroupChange }
                      onNumberOfDiceSliderChange={ this.onNumberOfDiceSlideChange }
                      onNumberOfDiceTextViewChanged={ this.onNumberOfDiceTextViewChanged } />
                    <GroupedByCombinations numberOfDice={ number } />
                  </div>
                );
              } else {
                return null;
              }
          }} />
        <Route exact path="/IDF/Training/XWingAttackDice" render={
          () => <Redirect to="/IDF/Training/XWingAttackDice/combinations/2" />
        } />
      </div>
    )
  }
}
