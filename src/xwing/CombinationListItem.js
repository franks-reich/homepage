import React from 'react';


import { DiceList } from './DiceList';


import { Icon } from 'office-ui-fabric-react/lib/Icon';


export const CombinationListItem = (props) => {
  const index = props.index;
  const length = props.length;
  const sides = props.sides;
  if (index === length - 1) {
    return (
      <div key={ index }
           className="combination-max-combination-sides">
        <DiceList sides={ sides } />
      </div>
    );
  } else {
    return (
      <div key={ index }
           className="combination-max-combination-sides">
        <DiceList sides={ sides } />
        <Icon className="combination-max-combination-sides-icon"
              iconName="Shield" />
      </div>
    );
  }
}
