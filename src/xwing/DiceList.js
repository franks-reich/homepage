import React from 'react';


import { Die } from './Die';


export const DiceList = (props) => {
  const dice = props.sides.map((side, index) => (
    <Die key={ index }
         text={ side } />
  ))
  return (
    <div
      className="attack-die-side-container-div">

      { dice }
    </div>
  );
}
