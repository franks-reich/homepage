import React from 'react';


import { Summary } from './Summary';
import { GraphBar } from './GraphBar';


import './Combination.css';


export const Combination = (props) => {
  const max = props.max;
  const item = props.item;
  return (
    <div className="combination-container">
      <GraphBar value={ item.count } max={ max } />
      <Summary item={ item } />
    </div>
  );
}
