import React from 'react';


import { Outcome } from './Outcome';


export const Outcomes = (props) => {
  const maxCount = Math.max(...props.outcomes.map(outcome => outcome.count));
  const outcomes = props.outcomes.map((outcome, index) => (
    <Outcome 
      key={ index }
      outcome={ outcome }
      maxCount={ maxCount } />
  ));
  return outcomes;
}
