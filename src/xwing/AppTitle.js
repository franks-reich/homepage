import React from 'react';


export const AppTitle = (props) => (
  <div className="combination-header-title">
    <h1 className="combination-header-title-text ms-font-su">
      { props.title }
    </h1>
    <h1 className="combination-header-title-count ms-font-su">
      { props.number }
    </h1>
  </div>
)
