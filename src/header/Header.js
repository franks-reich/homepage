import React from 'react';


import { NavigationBox } from './NavigationBox';


import './Header.css';


export class Header extends React.Component {
  constructor(props) {
    super(props);
    const key = props.location.pathname.split('/')[1];
    this.state = {
      activeKey: key,
      navigationBoxOpen: false
    };
    this.onNavigationItemClicked = this.onNavigationItemClicked.bind(this);
    this.onTileClicked = this.onTileClicked.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location !== this.props.location) {
      this.setState({
        activeKey: nextProps.location.pathname.split('/')[1],
        navigationBoxOpen: false
      })
    }
  }

  onNavigationItemClicked(event, key) {
    const open = this._shouldOpen(key);
    this.setState({
      activeKey: key,
      navigationBoxOpen: open
    });
  }

  _shouldOpen(key) {
    if (key === this.state.activeKey && this.state.navigationBoxOpen) {
      return false;
    } else {
      return true
    }
  }

  onTileClicked(event) {
    this.setState({ navigationBoxOpen: false });
  }

  render() {
    const sitemap = this.props.sitemap;
    const activeKey = this.state.activeKey;
    const navigationBoxOpen = this.state.navigationBoxOpen;
    return (
      <header className="Header-main ms-bgColor-themeDarker">
        <h1 className="header-title ms-font-su ms-fontColor-themeLighterAlt">
          Frank's Reich
        </h1>
        <nav>
          <NavigationBox sitemap={ sitemap }
                         activeKey={ activeKey }
                         open={ navigationBoxOpen }
                         onNavigationItemClicked={ this.onNavigationItemClicked }
                         onTileClicked={ this.onTileClicked } />
        </nav>
      </header>
    );
  }
}
