import React from 'react';


import './NavigationBar.css';


export const NavigationBar = (props) => {
    const sitemap = props.sitemap;
    const items = sitemap.firstLevel();
    const onNavigationItemClicked = props.onNavigationItemClicked;
    const activeKey = props.activeKey;
    const mainNav = items.map((item, index) => {
        if (item.key === activeKey) {
            return (
                <button className="navigation-box-nav-button"
                        key={ index.toString() }
                        onClick={ (e) => (onNavigationItemClicked(e, item.key)) }>
                    <h2 className="navigation-box-nav-h2 ms-font-xl ms-fontColor-themeLighterAlt">
                        { item.name }
                    </h2>
                </button>
            );
        } else {
            return (
                <button className="navigation-box-nav-button"
                        key={ index.toString() }
                        onClick={ (e) => (onNavigationItemClicked(e, item.key)) }>
                    <h3 className="navigation-box-nav-h3 ms-font-l ms-fontColor-themeLighterAlt">
                        { item.name }
                    </h3>
                </button>
            );
        }
    });
    return (
        <div className="navigation-box-main-nav">
            { mainNav }
        </div>
    )
};
