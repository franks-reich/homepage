import React from 'react';


import { TileBox } from './TileBox';


export const TileBoxes = (props) => {
    const onTileClicked = props.onTileClicked;
    const items = props.items;
    const tileBoxes = items.map((item, index) => (
        <TileBox item={ item } key={ index.toString() }
                 onTileClicked={ onTileClicked } />
    ));
    return (
        <div className="navigation-box-inside-box ms-bgColor-themeLighter">
            { tileBoxes }
        </div>
    );
};
