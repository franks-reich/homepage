import React from 'react';


import { NavigationBar } from './NavigationBar';
import { TileBoxes } from './TileBoxes';


import './NavigationBox.css';


export const NavigationBox = (props) => {
    const onTileClicked = props.onTileClicked;
    const key = props.activeKey;
    const sitemap = props.sitemap;
    const activeItem = sitemap.find(key);
    const open = (() => {
        if (activeItem.items.length === 0) {
            return false;
        } else {
            return props.open;
        }
    })();
    const onNavigationItemClicked = props.onNavigationItemClicked;
    if (open) {
        return (
            <div className="navigation-box-outside-box">
                <NavigationBar activeKey={ activeItem.key }
                               sitemap={ sitemap }
                               onNavigationItemClicked={ onNavigationItemClicked } />
                <TileBoxes items={ activeItem.items }
                           onTileClicked={ onTileClicked } />
            </div>
        );
    } else {
        return (
            <div className="navigation-box-outside-box-closed">
                <NavigationBar activeKey={ activeItem.key }
                               sitemap={ sitemap }
                               onNavigationItemClicked={ onNavigationItemClicked } />
            </div>
        );
    }
};
