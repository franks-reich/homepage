import React from 'react';


import { Link } from 'react-router-dom';


import './Tile.css';


export const Tile = (props) => {
    const onTileClicked = props.onTileClicked;
    const url = '/' + props.item.key;
    return (
        <Link className="navigation-box-tile ms-bgColor-themeDarker"
              key={ props.item.key }
              to={ url }
              onClick={ onTileClicked }>
            <p className="navigation-box-tile-text ms-font-m ms-fontColor-themeLighterAlt">
                { props.item.name }
            </p>
        </Link>
    )
};
