import React from 'react';


import { Tile } from './Tile';


export const TileBox = (props) => {
    const item = props.item;
    const onTileClicked = props.onTileClicked;
    const tiles = item.items.map((item, index) => (
        <Tile item={ item }
              key={ index.toString() }
              onTileClicked={ onTileClicked }/>
    ));
    return (
        <div>
            <h3 className="navigation-box-h3 ms-font-m-plus">
                { item.name }
            </h3>
            <div className="navigation-box-tile-block">
                { tiles }
            </div>
        </div>
    );
};
