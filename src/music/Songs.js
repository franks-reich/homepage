import React from 'react';


import { CommandBarButton } from 'office-ui-fabric-react/lib/Button';


import { Song } from './Song';


import './Songs.css';


export class Songs extends React.Component {
    render() {
        const songs = this.props.songs;
        const edit = this.props.edit;
        const songListToolbar = this.songListToolbar(edit);
        const songList = songs.map((song, index) => (
            <Song index={ index }
                  song={ song }
                  edit={ edit }
                  key={ index } />
        ));
        return (
            <React.Fragment>
                { songListToolbar }
                { songList }
            </React.Fragment>
        );
    }

    songListToolbar(edit) {
        if (edit) {
            return (
                <div className="emperors-music-album-songs-list-toolbar ms-bgColor-neutralLight">
                    <CommandBarButton className="emperors-music-songs-list-toolbar-button"
                                    iconProps={ { iconName: "Add" } }
                                    text={ "Add" }
                                    title={ "Add song" } />
                </div>
            );
        } else {
            return null;
        }
    }
}
