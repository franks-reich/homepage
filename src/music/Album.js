import React from 'react';


import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';


import { Songs } from './Songs';


import './Album.css';


export class Album extends React.Component {
    render() {
        const album = this.props.album;
        const albumTitle = this.albumTitle(album);
        const albumBottomToolbar = this.albumBottomToolbar(album);
        return (
            <div className="emperors-music-album-container ms-bgColor-themeLighterAlt ms-borderColor-themeTertiary">
                { albumTitle }
                <div className="emperors-music-album-inner-container">
                    <div className="emperors-music-album-image-container ms-bgColor-themeTertiary">
                    </div>
                    <div className="emperors-music-album-songs-list ms-bgColor-neutralLighterAlt ms-borderColor-themeTertiary">
                        <Songs songs={ album.songs } edit={ album.edit } />
                    </div>
                </div>
                { albumBottomToolbar }
            </div>
        );
    }

    albumBottomToolbar(album) {
        if (album.edit) {
            return (
                <div className="emperors-music-album-bottom-toolbar">
                    <DefaultButton className="emperors-music-bottom-toolbar-button"
                                   primary={ true }
                                   iconProps={ { iconName: "Save" } }
                                   text="Save"
                                   title="Save album" />
                    <DefaultButton className="emperors-music-bottom-toolbar-button"
                                   iconProps={ { iconName: "Cancel" } }
                                   text="Cancel"
                                   title="Cancel changes" />
                </div>
            );
        } else {
            return null;
        }
    }

    albumTitle(album) {
        if (album.edit) {
            return <TextField className="emperors-music-album-title-text-field" />
        } else {
            return <h2 className="emperors-music-album-title ms-font-xl">{ album.title }</h2>;
        }
    }
}
