import React from 'react';


import * as QueryString from 'query-string';


import { Albums } from './Albums';
import { AdminToolbar } from './AdminToolbar';


import './Music.css';


export class Music extends React.Component {
  constructor(props) {
    super(props);
    this.state = { albums: [] };
    this.addAlbum = this.addAlbum.bind(this);
  }

  componentDidMount() {
    fetch("/albums")
      .then(response => response.json())
      .then(data => this.setState({ albums: data._items }));
  }

  render() {
    const parameters = QueryString.parse(this.props.location.search);
    const admin = parameters.admin;
    const albums = this.state.albums;
    const adminToolbar = this._adminToolbar(admin);
    return (
      <div className="emperors-music-main">
        { adminToolbar }
        <p>
          Aside from ruling a country and leading the world to glory and
          freedom, the emperor still finds time for his dearest of hobbies
          - producing music.
        </p>
        <Albums albums={ albums } />
      </div>
    );
  }

  isAddAlbumButtonEnabled() {
    const filtered = this.state.albums.filter((album) => ( !album._id ));
    return filtered.length === 0;
  }

  addAlbum(event) {
    this.setState({
      albums: [{
        title: "",
        songs: [],
        edit: true
      }].concat(this.state.albums)
    });
  }

  _adminToolbar(admin) {
    if (admin) {
      const buttons = [
        {
          icon: "Add",
          text: "Add",
          tooltip: "Add an album",
          onClick: this.addAlbum,
          enabled: this.isAddAlbumButtonEnabled()
        }
      ];
      return <AdminToolbar buttons={ buttons } />;
    } else {
      return null;
    }
  }
};
