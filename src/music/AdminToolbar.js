import React from 'react';


import { CommandBarButton } from 'office-ui-fabric-react/lib/Button';


export const AdminToolbar = (props) => {
    const buttons = props.buttons;
    const buttonComponents = buttons.map((button, index) => (
        <CommandBarButton className="emperors-music-toolbar-button"
                          disabled={ !button.enabled }
                          iconProps={ { iconName: button.icon } }
                          text={ button.text }
                          title={ button.tooltip }
                          onClick={ button.onClick }
                          key={ index } />
    ));
    return (
        <div className="emperor-music-toolbar ms-bgColor-themeLighterAlt ms-borderColor-themeDark">
            <h2 className="emperors-music-album-title ms-font-xl">
                Admin Toolbar
            </h2>
            <div className="emperors-music-inner-toolbar">
                { buttonComponents }
            </div>
        </div>
    );
};
