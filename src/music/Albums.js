import React from 'react';


import { Album } from './Album';


export const Albums = (props) => {
    const albums = props.albums;
    const albumList = albums.map((album, index) => {
        return (
            <Album album={ album }
                   key={ index } />
        );
    });
    return (
        <React.Fragment>
            { albumList }
        </React.Fragment>
    );
}
