import React from 'react';


import './Song.css';


export class Song extends React.Component {
    render() {
        const index = this.props.index;
        const song = this.props.song;
        const itemClassName = this.itemClassName(index);
        return (
            <div className={ itemClassName }>
                <div className="emperors-music-album-songs-list-title">
                    { song.title }
                </div>
                <div className="emperors-music-album-songs-list-time">
                    <span>{ song.length }</span>
                </div>
            </div>
        );
    }

    itemClassName(index) {
        if (index % 2 === 0) {
            return "emperors-music-album-songs-list-item ms-font-l ms-bgColor-neutralLight";
        } else {
            return "emperors-music-album-songs-list-item ms-font-l ms-bgColor-neutralLighter";
        }
    }
}
