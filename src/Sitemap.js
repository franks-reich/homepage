export class Sitemap {
  find(key) {
    return this.sitemap.find((item) => (
      item.key === key
    ));
  }

  firstLevel() {
    return this.sitemap;
  }

  allItemsForKey(key) {
    const keys = key.split("/").splice(0, 3);
    var fullKeys = [];
    keys.forEach((key, index) => {
      var fullKey = "";
      if (index > 0) {
        fullKey = fullKeys[index - 1] + '/' + key;
      } else {
        fullKey = key;
      }

      fullKeys.push(fullKey);
    });

    var items = [];
    fullKeys.forEach((fullKey, index) => {
      var item = null;
      if (index > 0) {
        const previousItem = items[index - 1];
        item = previousItem.items.find((item) => (
          item.key === fullKey
        ));
      } else {
        item = this.sitemap.find((i) => (
          i.key === fullKey
        ));
      }
      items.push(item);
    });
    return items;
  }

  sitemap = [
    {
      name: "Home",
      key: "",
      items: []
    },
    {
      name: "Interplanetary Defense Force",
      key: "IDF",
      items: [
        {
          name: "Training",
          key: "IDF/Training",
          items: [
            {
              name: "X-Wing Attack Dice Combinations",
              key: "IDF/Training/XWingAttackDice"
            }
          ]
        }
      ]
    },
    {
      name: "Arts and Sciences",
      key: "AAS",
      items: [
        {
          name: "Music",
          key: "AAS/Music",
          items: [
            {
              name: "The Emperor's Glorious Music",
              key: "AAS/Music/EmperorsMusic"
            },
            {
              name: "Synths and Sampler",
              key: "AAS/Music/Synths"
            }
          ]
        },
        {
          name: "Holidays and Celebrations",
          key: "AAS/Celebrations",
          items: [
            {
              name: "Halloween",
              key: "AAS/Celebrations/Halloween"
            }
          ]
        }
      ]
    },
    {
      name: "Technology",
      key: "TECH",
      items: [
        {
          name: "Distributed Systems",
          key: "TECH/DistributedSystems",
          items: [
            {
              name: "Web Architecture Primer",
              key: "TECH/DistributedSystems/WebArchitecturePrimer"
            }
          ]
        }
      ]
    }
  ];
}
