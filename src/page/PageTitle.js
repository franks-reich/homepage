import React from 'react';


import { Breadcrumb } from './Breadcrumb';


import './PageTitle.css';


export class PageTitle extends React.Component {
    render() {
        const sitemap = this.props.sitemap;
        const location = this.props.location;
        const width = this.props.width;
        const style = { width: width };
        return (
            <div className="ms-bgColor-themeDarkAlt" style={ style }>
                <Breadcrumb location={ location }
                            sitemap={ sitemap } />
            </div>
        );
    }
}
