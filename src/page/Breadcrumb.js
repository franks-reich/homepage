import React from 'react';


import { Icon } from 'office-ui-fabric-react/lib/Icon';


import './Breadcrumb.css';


export const Breadcrumb = (props) => {
    const sitemap = props.sitemap;
    const location = props.location;
    const breadcrumb = (() => {
        const key = location.pathname.substr(1);
        const items = sitemap.allItemsForKey(key);
        const pathElements = items.map((item) => (item.name));

        if (location.pathname === '/') {
            return (
                <a className="page-title-breadcrumb-a">
                    <h1 className="page-title-breadcrumb-h1 ms-font-xl ms-fontColor-themeLighterAlt">Home</h1>
                </a>
            );
        } else {
            return pathElements.map((element, index) => {
                if (index < pathElements.length - 1) {
                    return (
                        <a className="page-title-breadcrumb-a"
                           key={ index.toString() }>
                            <h1 className="page-title-breadcrumb-h1 ms-font-xl ms-fontColor-themeLighterAlt">
                                { element }
                            </h1>
                            <Icon className="page-title-breadcrumb-icon ms-fontColor-themeLighterAlt"
                                iconName="Shield" />
                        </a>
                    );
                } else {
                    return (
                        <a className="page-title-breadcrumb-a"
                           key={ index.toString() }>
                            <h1 className="page-title-breadcrumb-h1 ms-font-xl ms-fontColor-themeLighterAlt">{ element }</h1>
                        </a>
                    );
                }
            });
        }
    })();
    return (
        <div className="page-title-breadcrumb">
            { breadcrumb }
        </div>
    );
}