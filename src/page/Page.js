import React from 'react';
import Measure from 'react-measure';
import { Route } from 'react-router-dom';


import { PageTitle } from './PageTitle';
import { PageContent } from './PageContent';


import './Page.css';


export class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAttached: false,
      dimensionsContent: {
        width: -1,
        height: -1
      },
      dimensionsTitle: {
        width: -1,
        height: -1
      }
    };
    this.onScroll = this.onScroll.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll);
  }

  onScroll(event) {
    const scrollOffset = this.props.scrollOffset;
    const scrollY = window.scrollY;
    const isAttached = (() => {
      if (scrollY > scrollOffset) {
        return true;
      } else {
        return false;
      }
    })();
    this.setState({ isAttached: isAttached });
  }

  className(isAttached) {
    if (isAttached) {
      return "page-title-attached";
    } else {
      return "page-title";
    }
  }

  render() {
    const sitemap = this.props.sitemap;
    const width = this.state.dimensionsContent.width;
    const isAttached = this.state.isAttached;
    const titleHeight = this.state.dimensionsTitle.height;
    const className = this.className(isAttached);
    return (
      <main>
        <Measure
          bounds
          onResize={(contentRect) => {
            this.setState({ dimensionsTitle: contentRect.bounds })
          }}>
          {({ measureRef }) =>
            <div className={ className } ref={ measureRef }>
              <Route
                path="/" render={
                (props) => (
                  <PageTitle
                    width={ width }
                    location={ props.location }
                    sitemap={ sitemap } />
                )
              }/>
            </div>
          }
        </Measure>
        <Measure
          bounds
          onResize={(contentRect) => {
              this.setState({ dimensionsContent: contentRect.bounds })
          }}>
          {({ measureRef }) =>
            <div className="main-content-div" ref={ measureRef }>
              <PageContent
                isAttached={ isAttached }
                titleHeight={ titleHeight } />
            </div>
          }
        </Measure>
      </main>
    );
  }
}
