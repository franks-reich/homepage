import React from 'react';
import Measure from 'react-measure';
import {
    BrowserRouter as Router, Route
} from 'react-router-dom';


import { Fabric } from 'office-ui-fabric-react/lib/Fabric';
import { initializeIcons } from 'office-ui-fabric-react/lib/Icons';


import { Header } from './header/Header';
import { Footer } from './footer/Footer';
import { Page } from './page/Page';
import { Sitemap } from './Sitemap';


import './App.css';


initializeIcons();


export class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dimensionsHeader: { height: -1 },
            sitemap: new Sitemap(),
            activeKey: ""
        }
    }

    render() {
        const headerHeight = this.state.dimensionsHeader.height;
        const scrollOffset = headerHeight;
        const sitemap = this.state.sitemap;
        return (
            <Fabric>
                <Router>
                    <div className="App">
                        <Measure bounds
                                 onResize={(contentRect) => {
                                     this.setState({ dimensionsHeader: contentRect.bounds })
                                 }}>
                            {({ measureRef }) =>
                                <div ref={ measureRef }>
                                    <Route path='/' render={(props) => (
                                        <Header sitemap={ sitemap }
                                                location={ props.location } />
                                    ) } />
                                </div>
                            }
                        </Measure>
                        <Page scrollOffset={ scrollOffset }
                              sitemap={ sitemap } />
                        <Footer />
                    </div>
                </Router>
            </Fabric>
        );
    }
}


export default App;
